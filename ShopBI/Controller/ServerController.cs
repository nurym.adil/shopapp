﻿using ShopBI.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.IO;

namespace ShopBI.Controller
{
    public class ServerController
    {
        public static HttpListener listener;
        public static string url = "http://localhost/";
        public ICollection<User> Users { get; set; } = new List<User>();

        public ServerController()
        {
            listener = new HttpListener();
            listener.Prefixes.Add(url);
        }
        public async Task Listen()
        {
            listener.Start();

            while (true)
            {
                try
                {
                    var context = await listener.GetContextAsync();
                    var request = context.Request;
                    var response = context.Response;
                    Console.WriteLine(request.RawUrl);

                    if (request.RawUrl == "/users/signup")
                    {
                        using var reader = new StreamReader(request.InputStream, request.ContentEncoding);
                        string text = reader.ReadToEnd();

                        var user = JsonConvert.DeserializeObject<User>(text);
                        var result = await Registration(user);
                        if (result)
                        {
                            await SendStringResponse(context.Response, "Регистрация прошло успешно");
                        }
                        else
                        {
                            await SendStringResponse(context.Response, "Такой пользователь существует!");
                        }
                    }
                    else if (request.RawUrl == "/users/signin")
                    {
                        using var reader = new StreamReader(request.InputStream, request.ContentEncoding);
                        string text = reader.ReadToEnd();

                        var user = JsonConvert.DeserializeObject<User>(text);
                        var result = await Login(user);
                        if (result == null)
                        {
                            await SendStringResponse(context.Response, "Вы неправильно ввели данные!");
                        }
                        else
                        {
                            context.Response.StatusCode = 1;
                            await SendStringResponse(context.Response, "Операция выполнено!");
                        }
                    }
                    else if (request.RawUrl == "/shop/catalog")
                    {
                     
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        public async Task<User> Login(User user)
        {
            using ShopContext shopContext = new ShopContext();

            var result = await shopContext.Users.Where(x => x.Login.Equals(user.Login) && x.Password.Equals(user.Password)).ToListAsync();
            if (result.Count == 0)
            {
                return null;
            }
            return result.FirstOrDefault();
        }
        public async Task<bool> Registration(User user)
        {
            using ShopContext shopContext = new ShopContext();

            var result = await shopContext.Users.Where(x => x.Login.Equals(user.Login)).ToListAsync();
            if (result.Count > 0)
            {
                return false;
            }

            await shopContext.AddAsync(user);
            await shopContext.SaveChangesAsync();
            return true;
        }
        public async Task SendStringResponse(HttpListenerResponse response, string text)
        {
            using var stream = response.OutputStream;
            var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(text));
            await stream.WriteAsync(buffer, 0, buffer.Length);

        }

    }
}
