﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopBI.Model
{
    public class Product : Entity
    {
        public string Name { get; set; }

        public double Price { get; set; }
        public virtual Category Category { get; set; }
    }
}
