﻿using System.ComponentModel.DataAnnotations;

namespace ShopBI.Model
{
    public class User : Entity
    {
        [Required]
        public string Name { get; set; }
        public string Login { get; set; }//Email
        public string Password { get; set; }
        public Cart  Cart { get; set; }
        public PurchaseHistory Orders { get; set; }//уже куппленные предметы
    }
}