﻿using System.Collections.Generic;

namespace ShopBI.Model
{
    public class PurchaseHistory:Entity
    {
        public User User { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}